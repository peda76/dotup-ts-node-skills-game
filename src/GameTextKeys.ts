export enum GameText {
  AtLeast_0_ButtonsPerPlayer = 'AtLeast_0_ButtonsPerPlayer', // 'you need at least {0} registered buttons per player',
  AtMost_0_ButtonsPerPlayer = 'AtMost_0_ButtonsPerPlayer', // = 'you can use a maximum of {0} registered buttons per player',
  AtLeast_0_Player = 'AtLeast_0_Player', // 'you need at least {0} players',
  AtMost_0_PerPlayer = 'AtMost_0_PerPlayer', // = 'you can use a maximum of {0} player',
  AtLeast_0_RoundsToPlay = 'AtLeast_0_RoundsToPlay', // 'you need at least {0} rounds to play',
  AtMost_0_RoundsToPlay = 'AtMost_0_RoundsToPlay' // 'you can use a maximum of {0} rounds to play'
  // AtLeast_0_ButtonsPerPlayer = 'you need at least {0} registered buttons per player',
  // AtMost_0_ButtonsPerPlayer = 'you can use a maximum of {0} registered buttons per player',
  // AtLeast_0_Player = 'you need at least {0} players',
  // AtMost_0_PerPlayer = 'you can use a maximum of {0} player',
  // AtLeast_0_RoundsToPlay = 'you need at least {0} rounds to play',
  // AtMost_0_RoundsToPlay = 'you can use a maximum of {0} rounds to play'

}
