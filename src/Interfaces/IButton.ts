import { ButtonColor } from '../Enumerations';

export interface IButton {
  id?: string;
  name: string;
  color?: ButtonColor;
}
